import { LightningElement, track } from 'lwc';

import {
    NavigationMixin
} from 'lightning/navigation';

//Apex Controller
import getCertification from '@salesforce/apex/DocumentController.getCertification';

export default class PreviewDocument extends NavigationMixin(LightningElement) {

    @track certificate;
    @track documentData = [];
    @track componentLoaded = false;
    @track showSpinner = false

    connectedCallback(){
        this.doinit();
    }

    doinit(){
        getCertification()
        .then(result => {
            this.showSpinner = true;
            if(result){
                this.certificate = result;
                this.certificate.forEach(element => {
                    let obj = {};
                    obj.id = element.contentDocumentId;
                    obj.contentVersionId = element.contentVersionId;
                    obj.url = element.previewUrl+element.contentVersionId;
                    obj.description = element.description;
                    this.documentData.push(obj);
                });
                this.componentLoaded = true;

            }
        })
        .catch(error => {
            console.log('error '+JSON.stringify(error));
        })
        .finally(()=> this.showSpinner = false);
    }
}