public with sharing class DocumentController {
    @AuraEnabled
    public static contentVersionWrapper getCertification(){
        List<Id> contentDocumentLinksIds = new List<Id>();
        List<ContentDocumentLink> linksList = new List<ContentDocumentLink>();
        List<contentVersionWrapper> cvWrapperList = new List<contentVersionWrapper>();
        List<Documents__c> docRecList = selectById();
        List<ContentVersion> contentVersionList = new List<ContentVersion>();
        
        if(docRecList.size()>0){
            for(Documents__c doc : docRecList){
                linksList.addAll(doc.ContentDocumentLinks);
            }
            for(ContentDocumentLink record : linksList){
                contentDocumentLinksIds.add(record.ContentDocumentId);
            }
            contentVersionList = selectContentVersionByContentDocIds(contentDocumentLinksIds);
            if(contentVersionList.size()>0){
                for(ContentVersion cv : contentVersionList){
                    contentVersionWrapper wrapper = new contentVersionWrapper();
                    wrapper.baseUrl = System.Label.Base_Url; //Base url of the portal
                    wrapper.previewUrl = 'sfc/servlet.shepherd/version/renditionDownload?rendition=THUMB720BY480&versionId=';
                    wrapper.contentDocumentId = cv.ContentDocumentId;
                    wrapper.contentVersionId = cv.Id;
                    wrapper.fileType = cv.FileType;
                    wrapper.description = cv.Description;
                    cvWrapperList.add(wrapper);
                }
                return cvWrapperList;
            }
        }
        return null;
    }

    public static List<Documents__c> selectById(){
        return [Select id,Name,Document_Type__c,Application__c,
                (Select Id,ContentDocumentId FROM ContentDocumentLinks) 
                FROM Documents__c WITH SECURITY_ENFORCED];
    }

    public static List<ContentVersion> selectContentVersionByContentDocIds(List<Id> contentDocumentLinksIds){
        return[ SELECT Id, ContentDocumentId, PathOnClient,ContentBodyId, VersionNumber, Title, Description, FileType, VersionData, FileExtension,  ContentLocation, TextPreview,Document_Type__c 
                FROM ContentVersion 
                WHERE ContentDocumentId IN :contentDocumentLinksIds WITH SECURITY_ENFORCED];
    }

    public class contentVersionWrapper{
        @AuraEnabled public String baseUrl;
        @AuraEnabled public String previewUrl;
        @AuraEnabled public String downloadUrl;
        @AuraEnabled public String fileType;
        @AuraEnabled public String contentDocumentId;
        @AuraEnabled public String contentVersionId;
        @AuraEnabled public String title;
    }
}
